# Using Workload Identity

A demo to show how to use [Workload Identity](https://cloud.google.com/kubernetes-engine/docs/concepts/workload-identity) to call Google Cloud API. In this demo we will call the [Translate API](https://cloud.google.com/translate) from a GKE application(pod) using Workload Identity.

Check the [Documentation](https://kameshsampath.gitlab.io/workload-identity-gke-demo) on how to setup GKE and deploy the demo application.

## License

[Apache License](./../LICENSE)
