The service should fail with a message,

```text
{"message":"Internal Server Error"}
```

When you check the logs of the `lingua-greeter` pod,

```shell
task app_logs
```

You should see a message like,

```text
time="2023-01-25T10:26:50Z" level=error msg="googleapi: Error 401: Request had invalid authentication credentials. Expected OAuth 2 access token, login cookie or other valid authentication credential. See https://developers.google.com/identity/sign-in/web/devconsole-project.\nMore details:\nReason: authError, Message: Invalid Credentials\n"
```

As it describes you don't have authentication credentials to call the API. All Google Cloud API requires `GOOGLE_APPLICATION_CREDENTIALS` to allow client to authenticate itself before calling the API.