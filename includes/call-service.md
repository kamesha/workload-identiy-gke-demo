
```shell
export SERVICE_IP=$(kubectl get svc -n demo-apps lingua-greeter -ojsonpath="{.status.loadBalancer.ingress[*].ip}")
```

Call the service to return the translation of `Hello World!` in [Tamil(ta)](https://en.wikipedia.org/wiki/Tamil_language){target=_blank},

```shell
curl "http://$SERVICE_IP/ta"
```
