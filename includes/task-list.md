
| Task | Description|
|:-----:  | :----------------------------------------------|
| app_logs |                    Shows the application logs
| call_service |                Calls the lingua-greeter service
| clean |                       Clean all terraform artifacts/assets
| create_cluster |              Create the GKE cluster
| default |                     Creates GKE cluster and Deploys the lingua-greeter application
| deploy_app |                  Deploy the application without workload identity      (aliases: deploy)
| destroy |                     Destroys terraform resources
| format |                      Format terraform files
| gitops-app-source |           Creates the fluxcd GitOps GitRepository Source of the demo application "lingua-greeter".      (aliases: app-source)
| gitops-app-kustomize |       Generate the flux kustomization resource to deploy the application                            (aliases: app-kustomize)
| init |                        Init terraform working directory
| restart_app |                 Restarts the kubernetes deployment
| show_service_ip |             Display the lingua-greeter service's LoadBalancer IP
| tfdocs |                      Build the terraform documentation
| validate |                    Validate the terraform resources
