### On Google Cloud 

The `task create_cluster` terraform script that we ran earlier to create the GKE infrastructure did the following,

- [x] Create a Service Account(SA) that has permissions to call Google Translation API, in our demo we call that SA as `translator`
- [x] Add `translator` SA with role `roles/cloudtranslate.user`
- [x] Add an [IAM binding policy](https://cloud.google.com/iam/docs/reference/rest/v1/Policy){target=_blank} to `translator` SA, with the role `roles/iam.workloadIdentityUser` and a member `"serviceAccount:$GOOGLE_CLOUD_PROJECT.svc.id.goog[demo-apps/lingua-greeter]"` (default workload identity SA)

### Kubernetes

For Kubernetes applications to leverage Workload Identity, we need to

- [x] Add `nodeSelector` to Kubernetes deployment `lingua-greeter` with a value `iam.gke.io/gke-metadata-server-enabled: "true"`. That enables Kubernetes to schedule the pod on the nodes where Workload Identity is enabled
- [x] Annotate the KSA  `lingua-greeter` with `iam.gke.io/gcp-service-account: translator@$GCP_PROJECT.iam.gserviceaccount.com` to allow it to impersonate as `translator` service account