# Download Tools

We will be using the following tools as part of the tutorial. Please have them installed and configured before proceeding further.

| Tool                   |   URL                        |
| :-----------------:    | :---------------------------|
| Google Cloud SDK | <https://cloud.google.com/sdk>{target=_blank}
| GitLab CLI | <https://gitlab.com/gitlab-org/cli#installation>{target=_blank}
| terraform | <https://terraform.build> {target=_blank}
| kubectl | <https://kubernetes.io/docs/tasks/tools> {target=_blank}
| helm | <https://helm.sh> {target=_blank}
| flux | <https://fluxcd.io/flux/cmd/> {target=_blank}
| Taskfile | <https://taskfile.dev>{target=_blank}
| kustomize(Optional) | <https://kustomize.io>{target=_blank}
| direnv | <https://direnv.net>{target=_blank}
