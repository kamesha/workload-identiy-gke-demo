*[WAF]: Web Application Firewall
*[SPA]: Single Page Application
*[CORS]: Cross-Origin Resource Sharing
*[REST]: Representational state transfer
*[AWS]: Amazon Web Services
*[CSI]: Container Storage Interface
*[EBS]: Elastic Block Storage
*[PVC]: Persistent Volume Claim
*[mTLS]: Mutual Transport Layer Security
*[SSL]: Secure Sockets Layer
*[CSR]: Certificate Signing Request
*[VPN]: Virtual Private Network
*[IPSec]: IP Security
*[GKE]: Google Kubernetes Engine
*[IKEV2]: Internet Key Exchange v2
*[API]: Application Programming Interface
*[ACL]: Access Control List
*[GCS]: Google Cloud Storage
*[GSA]: Google Service Account
*[IAM]: Identity and Access Management
*[KSA]: Kubernetes Service Account
*[RBA]: RolBased Access Control
*[SA]:  Service Account
*[VPC]: Virtual Private Cloud
*[GAR]: Google Artifact Registry
*[OCI]: Open Containers Initiative
*[CI]: Continuous Integration
*[CD]: Continuous Deployment
