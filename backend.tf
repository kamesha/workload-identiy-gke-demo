# uncomment this if you want to use GitLab to save the state
# terraform {
#   backend "http" {
#   }
# }
terraform {
  cloud {
    organization = "kameshsampath"

    workspaces {
      name = "workload-identity-demo"
    }
  }
}

