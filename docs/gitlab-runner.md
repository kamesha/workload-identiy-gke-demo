---
title: Gitlab Runner
summary: Deploy and Register GitLab runner
authors:
  - Kamesh Sampath
date: 2023-03-22
---

At the end of this chapter you would have deployed a self-managed [GitLab Runner](https://docs.gitlab.com/runner/) on GKE.

## Overview

We will use `terraform` to automate the deployment of [GitLab Runner](https://docs.gitlab.com/runner/install/kubernetes.html){target=_blank} on [Google Kubernetes Engine(GKE)](https://cloud.google.com/kubernetes-engine){target=_blank}.

!!!important
    We use self managed runner to enable GitLab pipelines use Workload Identity. It also helps us avoid exhausting the free tier time limits(400 mins/mo) provided by GitLab for using shared runners.
  
## Download Template Repository

Using `glab` cli download and extract the GKE GitLab Runner setup scripts.

Open a new terminal run the following command to create a `work` directory,

```shell
mkdir -p $DEMO_HOME/work
cd $DEMO_HOME/work
```

!!!note
    `$DEMO_HOME` is the directory where you have forked the repository `workload-identity-gke-demo`.

Download and unzip template repository,

```shell
glab repo archive https://gitlab.com/kameshsampath/runner-gke --format=tar.gz
unzip -q -d runner-gke -j kameshsampath-runner-gke.zip
cd runner-gke
export RUNNER_HOME="$PWD"
```

## Environment Variables

When working with Google Cloud the following environment variables helps in setting the right Google Cloud context like Service Account Key file, project etc., You can use [direnv](https://direnv.net) or set the following variables on your shell,

```shell
export GOOGLE_APPLICATION_CREDENTIALS="the google cloud service account key json file to use"
export CLOUDSDK_ACTIVE_CONFIG_NAME="the google cloud cli profile to use"
export KUBECONFIG="$RUNNER_HOME/.kube"
```

!!! note
    `GOOGLE_APPLICATION_CREDENTIALS` is the SA json with following roles and will be used to create the GKE Infra and deploy GitLab Runner.

    - `Kubernetes Engine Admin`  - to create GKE cluster
    - `Service Account User`     - to use other needed service accounts
    - `Service Account Admin`    - to use create new service accounts
    - `Project IAM Admin`        - to create/update IAM Policy Binding
    - `Compute Network Admin`    - to create the VPC networks that will be used by the GitLab Runner GKE

You can find more information about gcloud cli configurations at <https://cloud.google.com/sdk/docs/configurations>.

As you may need to override few terraform variables that you don't want to check in to VCS, add them to a file called `.local.tfvars` and set the following environment variable to be picked up by terraform runs,

```shell
export TFVARS_FILE=.local.tfvars
```

>**NOTE**: All `.local.tfvars` file are git ignored by this template.

## Terraform Configuration

### Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | The GKE cluster name | `string` | `"gitlab-runner"` | no |
| <a name="input_gcp_project"></a> [gcp\_project](#input\_gcp\_project) | The Google Cloud Project | `any` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | The Google Cloud Region where the cluster will be created | `string` | `"asia-south1"` | no |
| <a name="input_gitlab_runner_namespace"></a> [gitlab\_runner\_namespace](#input\_gitlab\_runner\_namespace) | The GitLab Runner Namespace | `string` | `"gitlab-runner"` | no |
| <a name="input_gitlab_runner_registration_token"></a> [gitlab\_runner\_registration\_token](#input\_gitlab\_runner\_registration\_token) | The GitLab Runner Registration Token | `string` | n/a | yes |
| <a name="input_gitlab_runner_replicas"></a> [gitlab\_runner\_replicas](#input\_gitlab\_runner\_replicas) | The GitLab Runner Replicas | `number` | `1` | no |
| <a name="input_gitlab_runner_sa"></a> [gitlab\_runner\_sa](#input\_gitlab\_runner\_sa) | The GitLab Runner SA | `string` | `"gitlab-runner"` | no |
| <a name="input_gitlab_url"></a> [gitlab\_url](#input\_gitlab\_url) | The GitLab URL | `string` | `"https://gitlab.com"` | no |
| <a name="input_gke_num_nodes"></a> [gke\_num\_nodes](#input\_gke\_num\_nodes) | Number of GKE nodes | `number` | `2` | no |
| <a name="input_kubernetes_version"></a> [kubernetes\_version](#input\_kubernetes\_version) | The kubernetes versions of the GKE clusters | `string` | `"1.24."` | no |
| <a name="input_machine_type"></a> [machine\_type](#input\_machine\_type) | The Google Cloud machine types for each cluster node | `string` | `"n2-standard-4"` | no |
| <a name="input_release_channel"></a> [release\_channel](#input\_release\_channel) | The GKE release channel to use | `string` | `"stable"` | no |

### Outputs

| Name | Description |
|------|-------------|
| <a name="output_env-dynamic-url"></a> [env-dynamic-url](#output\_env-dynamic-url) | GKE Cluster Endpoint URL |
| <a name="output_gitlab-runner-namespace"></a> [gitlab-runner-namespace](#output\_gitlab-runner-namespace) | The namespace where gitlab-runner is deployed |
| <a name="output_kubernetes-cluster-name"></a> [kubernetes-cluster-name](#output\_kubernetes-cluster-name) | GKE Cluster Name |
| <a name="output_project-name"></a> [project-name](#output\_project-name) | Google Cloud Project Name |
| <a name="output_region"></a> [region](#output\_region) | Google Cloud Region where the cluster is provisioned |
| <a name="output_zone"></a> [zone](#output\_zone) | Google Cloud Zone where the cluster is provisioned |

## Tasks

Project uses [taskfile](https://taskfile.dev) for executing commands.

| Task  | Description
| :----:| :----------------------------------
| clean |                Clean all terraform artifacts/assets
| create_cluster |       Create the GKE cluster      (aliases: create, deploy)
| default |              Creates GKE cluster and Deploys the lingua-greeter application
| destroy |              Destroys terraform resources      (aliases: delete, cleanup)
| format |               Format terraform files
| get_kubeconfig |       Get kubeconfig of the cluster      (aliases: kubeconfig)
| init |                 Init terraform working directory
| readme |               Build the README
| validate |             Validate the terraform resources

## Deploy Runner

Create a `.local.tfvars` with the following values,

```shell
gcp_project          = "REPLACE WITH YOUR GCP PROJECT"
gitlab_runner_registration_token = "REPLACE WITH YOUR GITLAB RUNNER TOKEN"
```

Running the following command will create the GKE,

```shell
task create_cluster
```

>**NOTE**: It will take approx 10mins or for the cluster creation to complete.

The `create_cluster` task will,

- [x] Create a GKE Cluster
- [x] Deploy GitLab Runner using Helm Chart 
- [x] Create GSA `gitlab-runner` with GCP roles
    - `roles/iam.workloadIdentityUser` - to allow Workload Identity
    - `roles/artifactregistry.createOnPushRepoAdmin` - push artifacts to GAR
    - `roles/storage.objectCreator` - to use GCS as runner cache

Once the deployment is successful you can verify the runner availability on your GitLab project.

An example of project scoped runner is shown below,

![Runner Connected](assets/images/gitlab_runner_connected.png)

### KubeConfig

Get the kubeconfig and save it to `$KUBECONFIG`,

```shell
task kubeconfig
```

You can check the `gitlab-runner` resources by running the command,

```shell
kubectl get all -n $(terraform output -raw gitlab-runner-namespace)
```

```shell
NAME                                 READY   STATUS    RESTARTS   AGE
pod/gitlab-runner-854478fc9b-xx9jc   1/1     Running   0          91m

NAME                            READY   UP-TO-DATE   AVAILABLE   AGE
deployment.apps/gitlab-runner   1/1     1            1           91m

NAME                                       DESIRED   CURRENT   READY   AGE
replicaset.apps/gitlab-runner-854478fc9b   1         1         1       91m
```

---8<-- "includes/abbrevations.md"
