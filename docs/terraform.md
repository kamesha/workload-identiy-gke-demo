The following sections details the inputs and outputs that are configurable as part of the tutorial's terraform automation,

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_app_ksa"></a> [app\_ksa](#input\_app\_ksa) | The kubernetes service account that will be used to run the lingua-greeter deployment | `string` | `"lingua-greeter"` | no |
| <a name="input_app_namespace"></a> [app\_namespace](#input\_app\_namespace) | The Kubernetes namespace where the lingua-greeter demo application will be deployed | `string` | `"demo-apps"` | no |
| <a name="input_app_use_workload_identity"></a> [app\_use\_workload\_identity](#input\_app\_use\_workload\_identity) | Flag to enable/disable application(pod) from using Workload Identity | `bool` | `false` | no |
| <a name="input_cluster_name"></a> [cluster\_name](#input\_cluster\_name) | The gke cluster name | `string` | `"wi-demos"` | no |
| <a name="input_gcp_project"></a> [gcp\_project](#input\_gcp\_project) | The google cloud project | `any` | n/a | yes |
| <a name="input_gcp_region"></a> [gcp\_region](#input\_gcp\_region) | The google cloud region where the cluster will be created | `string` | `"asia-south1"` | no |
| <a name="input_gitlab_agent_namespace"></a> [gitlab\_agent\_namespace](#input\_gitlab\_agent\_namespace) | Kubernetes namespace to install the Agent | `string` | `"gitlab-agent"` | no |
| <a name="input_gitlab_agent_token"></a> [gitlab\_agent\_token](#input\_gitlab\_agent\_token) | Agent token (provided when registering an Agent in GitLab) | `any` | n/a | yes |
| <a name="input_gitlab_kas_address"></a> [gitlab\_kas\_address](#input\_gitlab\_kas\_address) | Agent Server address (provided when registering an Agent in GitLab) | `any` | n/a | yes |
| <a name="input_gke_num_nodes"></a> [gke\_num\_nodes](#input\_gke\_num\_nodes) | Number of GKE nodes | `number` | `2` | no |
| <a name="input_kubernetes_version"></a> [kubernetes\_version](#input\_kubernetes\_version) | The kubernetes versions of the GKE clusters | `string` | `"1.24."` | no |
| <a name="input_machine_type"></a> [machine\_type](#input\_machine\_type) | The google cloud machine types for each cluster node | `string` | `"e2-standard-4"` | no |
| <a name="input_release_channel"></a> [release\_channel](#input\_release\_channel) | The GKE release channel to use | `string` | `"stable"` | no |

### Example
  
An example `.local.tfvars` that will use a Google Cloud project **my-awesome-project**, create a two node GKE cluster named **wi-demo** in region **asia-south1** with Kubernetes version **1.24.** from **stable** release channel. The machine type of each cluster node will be **e2-standard-4**. The demo will be deployed in Kubernetes namespace **demo-apps**, will use **lingua-greeter** as the Kubernetes Service Account.

```hcl
app_ksa            = "lingua-greeter"
app_namespace      = "demo-apps"
cluster_name       = "wi-demo"
configure_app_workload_identity = false
gke_num_nodes      = 2
kubernetes_version = "1.24."
machine_type       = "e2-standard-4"
project_id         = "my-awesome-project"
region             = "asia-south1"
release_channel    = "stable"
```

> **NOTE**: For rest of the section we assume that your tfvars file is called `.local.tfvars`
>

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_env-dynamic-url"></a> [env-dynamic-url](#output\_env-dynamic-url) | GKE Cluster Endpoint URL |
| <a name="output_ksa-patch"></a> [ksa-patch](#output\_ksa-patch) | The Kubernetes Service Account patch required for Workload Identity |
| <a name="output_kubernetes-cluster-name"></a> [kubernetes-cluster-name](#output\_kubernetes-cluster-name) | GKE Cluster Name |
| <a name="output_project-name"></a> [project-name](#output\_project-name) | Google Cloud Project Name |
| <a name="output_region"></a> [region](#output\_region) | Google Cloud Region where the cluster is provisioned |
| <a name="output_translator-service-account"></a> [translator-service-account](#output\_translator-service-account) | The Google Service Account 'translator' |
| <a name="output_zone"></a> [zone](#output\_zone) | Google Cloud Zone where the cluster is provisioned |

---8<-- "includes/abbrevations.md"
