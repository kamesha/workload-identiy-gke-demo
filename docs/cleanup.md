---
title: Cleanup
summary: Cleanup the demo environment
authors:
  - Kamesh Sampath
date: 2023-03-22
---

## Destroy Application GKE Cluster

Click **destroy** from the Project --> **CI/CD** -> **Pipelines** --> **Tags**, choose the tag that you used to create the demo environment and click *cleanup** to start `terraform` destroy to cleanup the GKE environment.

![Destroy Infra](assets/images/gitlab_pipelines_destroy.png)

Once the destroy is successful you can also stop the self-managed runner,

```shell
cd $DEMO_HOME
docker compose down
```

## Destroy GKE GitLab Runner Infra

Destroy the GKE GitLab runner infra,

```shell
cd $DEMO_HOME/work/runner-gke
task destroy
```
