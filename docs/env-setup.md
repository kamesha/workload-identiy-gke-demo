---
title: Environment Setup
summary: The environment setup required for the demo
authors:
  - Kamesh Sampath
date: 2023-03-22
---

The following pre-requisites are required to setup the Google Cloud environment required for this tutorial,

- [Google Cloud Account](https://cloud.google.com){target=_blank}
- Google Cloud[Service Account](https://cloud.google.com/iam/docs/service-account-overview){target=_blank} with roles:
  - **Kubernetes Engine Admin** - to create GKE cluster
  - **Service Account** roles used to create/update/delete other Service Account
    - iam.serviceAccounts.actAs
    - iam.serviceAccounts.get
    - iam.serviceAccounts.create
    - iam.serviceAccounts.delete
    - iam.serviceAccounts.update
    - iam.serviceAccounts.get
    - iam.serviceAccounts.getIamPolicy
    - iam.serviceAccounts.setIamPolicy
    - **Or** simply you can add **Service Account Admin** and **Service Account User** roles
  - **Compute Network Admin**   - to create/delete the VPC networks

## Environment Variables

When working with Google Cloud the following environment variables helps in setting the right Google Cloud context like Service Account Key file, project etc., You can use [direnv](https://direnv.net) or set the following variables on your shell,

```shell
export GOOGLE_APPLICATION_CREDENTIALS="the google cloud service account key json file to use"
export CLOUDSDK_ACTIVE_CONFIG_NAME="the google cloud cli profile to use"
export GOOGLE_CLOUD_PROJECT="the google cloud project to use"
export KUBECONFIG="$DEMO_HOME/.kube"
```

(e.g.)

```shell
export CLOUDSDK_ACTIVE_CONFIG_NAME=personal
export GOOGLE_APPLICATION_CREDENTIALS=~/.ssh/my-sa-key.json
export GOOGLE_CLOUD_PROJECT=my-awesome-project
export KUBECONFIG="$DEMO_HOME/.kube"
```

> **TIP** If you are using direnv you can then create file `.envrc.local` and add the environment variables. They can then be loaded using `direnv allow .`

You can find more information about gcloud cli configurations at <https://cloud.google.com/sdk/docs/configurations>.

As you may need to override few terraform variables that you don't want to check in to VCS, add them to a file called `.local.tfvars` and set the following environment variable to be picked up by terraform runs,

```shell
export TFVARS_FILE=.local.tfvars
```

!!!note
     All `.local.tfvars` file are git ignored by this template.

Check the [Inputs](#inputs) section for all possible variables that are configurable.
