---
title: Gitlab CI/CD
summary: Deploy the infrastructure and application using GitLab CI/CD
authors:
  - Kamesh Sampath
date: 2023-03-22
---

At the end of this chapter you would have deployed  the GKE infrastructure[^1] using [GitLab](https://gitlab.com) CI/CD and use it as the target infrastructure to deploy the applications.

## Demo Infrastructure

The demo repository is enabled to work with GitLab CI/CD via,

- `.gitlab/agents/gke-agent/config.yaml` - Allows the tutorial to use GKE Gitlab Agent
- `.gitlab-ci.yml` - Enables the use of GitLab CI/CD

We will use `terraform` to create the GKE cluster and use GitLab to manage its terraform state[^3].

Navigate back to the `$DEMO_HOME`,

```shell
cd $DEMO_HOME
```

## Variables

To deploy to our GKE Infrastructure using `terraform` and GitLab, we need the following variables to be configured on the **Settings > CI/CD** of the GitLab project.

![CI/CD Settings](assets/images/cicd_settings_vars.png)

### Google Cloud

- `BASE64_GOOGLE_CREDENTIALS` - The base64 encoded Google Service Account JSON key that will be used to create the cluster using `terraform`. This Service Account should have the roles/permissions as described in [Environment](env-setup.md#prepare) chapter.

### GitLab Agent

Register[^2] the GitLab Agent and set the configuration values in following variables,

- `TF_VAR_gitlab_agent_namespace` - The Kubernetes namespace where the GitLab agent will be deployed
- `TF_VAR_gitlab_agent_token`[^1] - The Kubernetes Agent Agent Token
- `TF_VAR_gitlab_kas_address`[^1] - The Kubernetes Agent Server Address

!!! note
      The GitLab Agent is deployed to the GKE using the following terraform script,

      ```hcl
      ---8<-- "gitlab-gke-agent.tf"
      ```

---8<-- "includes/abbrevations.md"

### GKE Terraform Configuration

Let use our `terraform` example variables file to under what values to set for the remaining variables,

```hcl
---8<-- "includes/examples/.local.tfvars.example"
```

- `TF_VAR_cluster_name` - The GKE cluster name e.g. `wi-demos`
- `TF_VAR_gcp_project` - The GKE cluster name e.g. `my-awesome-project`
- `TF_VAR_gcp_region`  - The GKE cluster name e.g. `asia-south-1`
- `TF_VAR_kubernetes_version` - The GKE cluster name e.g. `1.24.`

!!!important
    The terraform variables are configured and visible only to protected tags and branches

Please check the [documentation](https://docs.gitlab.com/ee/user/project/protected_tags.html){target=_blank} on how to protect your tags. The tutorial assumes that you have protected all `v*` branches.

## Create GKE Cluster

Infrastructure is deployed manually by clicking **deploy** from the Project --> **CI/CD** -> **Pipelines**,

![Deploy Infra](assets/images/gitlab_pipelines_deploy.png)

!!!note
    It will take 10-15 mins for the infrastructure to be provisioned.

Once the infrastructure is successfully provisioned, you will see the agent connected,

![Agent Connected](assets/images/gitlab_agent_connected.png)

Now we are set to deploy and test Workload Identity with our application.

## Initialize Terraform Locally

It is very handy and recommended to have your local terraform repo initialized with GitLab Terraform backend. Copy the terraform `init` command from your **Project** --> **Infrastructure** --> **Terraform**,

![Terraform Init Command](assets/images/terraform_init.png)

With local terraform state in sync with remote grab the GKE Cluster's `kubeconfig`,

```shell
task kubeconfig
```

!!!note
    The `terraform` state is stored in GitLab using `http` backend. For more information how terraform state management with GitLab, check the official [documentation](https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html/#initialize-a-terraform-state-as-a-backend-by-using-gitlab-cicd){target=_blank}

[^1]: https://docs.gitlab.com/ee/user/clusters/agent/install/

[^2]: <https://docs.gitlab.com/ee/user/infrastructure/clusters/connect/new_gke_cluster.html>{target=_blank}

[^3]: <https://docs.gitlab.com/ee/user/infrastructure/iac/index.html>{target=_blank}

---8<-- "includes/abbrevations.md"
