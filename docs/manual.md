---
title: Manual Deployment
summary: Run all the tasks of the tutorial from your local laptop.
authors:
  - Kamesh Sampath
date: 2023-03-22
---

This chapter details you the steps required to setup the demo environment and deploy the demo application from your local laptop.

## Tasks

The deployment is done using [task](https://taskfile.dev){target=_blank}. You can list all available tasks using the command,

```shell
task --list
```

---8<-- "includes/task-list.md"

## Create Environment

We will use terraform to create all the Google Cloud resources like GKE, Service Account, Kubernetes manifests etc.,

```shell
task init
```

### Create GKE cluster

The terraform apply will create the following Google Cloud resources,

- A Kubernetes cluster on GKE with **Workload Identity** enabled
- A Google Cloud VPC that will be used with GKE

```shell
task create_cluster
```

### Retrieve Kubeconfig

The following command should retrieve and save the `kubeconfig` at `$KUBECONFIG`,

```shell
gcloud container clusters get-credentials $(terraform output -raw kubernetes-cluster-name) --zone=$(terraform output -raw zone)
```
  
### Deploy Application

To see **Workload Identity** in action we will deploy the application in two parts,

- Application(workload) **not** enabled for **Workload Identity**
- Application(workload) **enabled** for **Workload Identity**

Run the following command to deploy the application,

```shell
kubectl apply -k "$DEMO_HOME/app"
```

Wait for application to be ready,

```shell
kubectl rollout status -n demo-apps deployment/lingua-greeter --timeout=60s
```

Wait for the application service LoadBalancer IP,

```shell
while [ -z $(kubectl get svc -n demo-apps lingua-greeter -ojsonpath="{.status.loadBalancer.ingress[*].ip}") ]; do sleep .3; done;
```

### Call Service

---8<-- "includes/call-service.md"

---8<-- "includes/service-error.md"

### Configure Application to use Workload Identity

---8<-- "includes/configure-workload-identity.md"

Edit `$DEMO_HOME/app/kustomization.yaml` and uncomment the highlighted lines to patch the KSA with GSA, this allows KSA to impersonate as GSA while calling Google Translate API:

```yaml hl_lines="19-29"
resources:
  - https://gitlab.com/kameshsampath/lingua-greeter//config?ref=main
patches:
  # Step 1
  - patch: |
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: lingua-greeter
      spec:
       template:
         spec:
            nodeSelector:
               iam.gke.io/gke-metadata-server-enabled: "true"
    target:
      kind: Deployment
      labelSelector: app=lingua-greeter
  # Step 2 - uncomment this patch to enable workload identity
  # - patch: |
  #     apiVersion: v1
  #     kind: ServiceAccount
  #     metadata:
  #       name: lingua-greeter
  #       annotations:
  #         iam.gke.io/gcp-service-account: translator@$GCP_PROJECT.iam.gserviceaccount.com
  #   target:
  #     kind: ServiceAccount
  #     name: lingua-greeter
  #     labelSelector: app=lingua-greeter
```

!!!important
    Ensure you update the **$GCP_PROJECT** with your Google Project Name.

Run the following command to update the Kubernetes SA `lingua-greeter` to use the Google IAM service Account using **Workload Identity mechanics**,

```shell
kubectl apply -k  "$DEMO_HOME/app"
```

You can verify the SA patch by running the command,

```shell
kubectl get sa -n demo-apps lingua-greeter -o yaml
```

Call the service again,

---8<-- "includes/call-service.md"

!!! note
    If you get the error response again, wait for few seconds for metadata refresh to happen.

---8<-- "includes/service-success.md"

For more information check out [Workload Identity](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity){target=_blank}.

## Cleanup

Run the following command to destroy the GKE cluster that was created for this demo.

```shell
task destory
```

If you plan to explore the same using [CI/CD](gitlab.md), then run the following command to cleanup the created resources,

```shell
kubectl delete -k app/
```

---8<-- "includes/abbrevations.md"
