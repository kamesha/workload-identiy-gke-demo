---
title: Tools and Sources
summary: Tools and demo sources that are required for this tutorial.
authors:
  - Kamesh Sampath
date: 2023-03-23
--- 

---8<--- "includes/tools.md"

## Pre-requisite

Ensure you have a [GitLab](https://gitlab.com) account.

## Download the Sources

Let us fork and clone the tutorial sources,

```shell
git clone https://gitlab.com/kameshsampath/workload-identity-gke-demo -b template --depth=1 workload-identity-gke-demo
cd workload-identity-gke-demo
glab repo fork
```

For rest of the tutorial we will call our tutorial folder as `$DEMO_HOME`,

```shell
export DEMO_HOME="$PWD"
```
