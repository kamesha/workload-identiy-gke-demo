---
title: GitOps
summary: Deploy Application using GitOps
authors:
  - Kamesh Sampath
date: 2023-03-22
---

In this chapter we will apply GitOps[^1] principles with [Flux CD](https://fluxcd.io) to deploy the demo application **lingua-greeter**.

## Demo Application Overview

As part of the demo, let us deploy a Kubernetes application called `lingua-greeter`. The application exposes a REST API `/:lang` , that allows you to translate a text `Hello World!` into the language `:lang` using Google Translate client.

!!!note
     The `:lang` is the BCP 47 language code, <https://en.wikipedia.org/wiki/IETF_language_tag>.

You can find the `lingua-greeter` application sources at <https://gitlab.com/kameshsampath/lingua-greeter>

## Pre-requisite

- Ensure you have [deployed](gitlab-runner.md#deploy-runner) the self-managed GitLab Runner which will be used to run CI pipelines
- Ensure you have [deployed](gitlab.md#deploy-infrastructure) GKE infrastructure to **lingua-greeter** demo application

## Create GAR Repository

The GitOps deployment will be using [Flux](https://fluxcd.io/flux/cheatsheets/oci-artifacts/) OCI[^2] repository as source of Kubernetes manifests.

Using your Google Cloud Web Console navigate to **Artifact Registry**, create a repository named `lingua-greeter` under your `$GCP_PROJECT`,

![GAR Repo](assets/images/gar_repo.png)

Edit the `.gitlab-ci.yml`  and update the variable `GAR_REGISTRY`  to the value matching your location.

e.g. From the above screenshot the `GAR_REGISTRY` will be set to `asia-south1-docker.pkg.dev` i.e `<your repo location>-docker.pkg.dev`.

Export the repository location as an environment variable,

```shell
export LINGUA_GREETER_MANIFEST_IMAGE_REPO=$GAR_REGISTRY/$GCP_PROJECT/lingua-greeter/manifests
```

## Enable Workload Identity For Flux

In the upcoming sections the flux `source-controller` will download the OCI image from `$LINGUA_GREETER_MANIFEST_IMAGE_REPO` to sync the manifests.

As part of the GKE Infrastructure deployment we have enabled Workload Identity and created a GSA called `gar-reader` which has permissions to download artifacts GAR.

Let us enable the flux `source-controller` to impersonate `gar-reader`.

Edit the `$DEMO_HOME/clusters/dev/flux-system/kustomization.yaml` and update it as shown,

```yaml hl_lines="10-20"
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
resources:
- gotk-components.yaml
- gotk-sync.yaml

patches:
  - patch: |
      apiVersion: v1
      kind: ServiceAccount
      metadata:
        name: source-controller
        annotations:
          iam.gke.io/gcp-service-account: gar-reader@$GCP_PROJECT.iam.gserviceaccount.com    
    target:
      kind: ServiceAccount
      name: source-controller
```

!!!note
    Make sure to update `$GCP_PROJECT` with your GCP project.

!!!tip
    You can also  use the following  command to patch `$DEMO_HOME/clusters/dev/flux-system/kustomization.yaml` with required patch
    ```shell
    task patch_flux_sa
    ``` 

Commit and push the code to allow flux to sync.

Watch the flux sync using the command

```shell
flux get kustomizations flux-system --watch
```

Once the sync is successful, run the following command to verify the KSA `source-controller` has been updated with annotation `iam.gke.io/gcp-service-account`,

```shell
kubectl get sa -n flux-system source-controller -o yaml
```

## Create OCI Source

Run the following command to create the application source to sync,

```shell
task gitops-app-source
```

The command should create file `lingua-greeter-source.yaml` under `$DEMO_HOME/clusters/dev/` with the following contents,

```yaml hl_lines="9-12"
---
apiVersion: source.toolkit.fluxcd.io/v1beta2
kind: OCIRepository
metadata:
  name: lingua-greeter
  namespace: flux-system
spec:
  interval: 10m0s
  provider: gcp
  ref:
    tag: latest
  url: oci://$GAR_REGISTRY/$GCP_PROJECT/lingua-greeter/manifests
```

The `url` will be the value of `$LINGUA_GREETER_MANIFEST_IMAGE_REPO`.

And the kustomization to deploy the application,

```shell
task app-kustomize
```

The command should create file `lingua-greeter-kustomization.yaml` under `$DEMO_HOME/clusters/dev/` with the following contents,

```yaml hl_lines="11-13"
---
apiVersion: kustomize.toolkit.fluxcd.io/v1beta2
kind: Kustomization
metadata:
  name: lingua-greeter
  namespace: flux-system
spec:
  interval: 5m0s
  path: ./config
  prune: true
  sourceRef:
    kind: GitRepository
    name: lingua-greeter
  targetNamespace: demo-apps
```

## Git Include FluxCD Resources

The files `lingua-greeter-source.yaml` and `lingua-greeter-kustomization.yaml` are ignored by git, edit the `$DEMO_HOME/.gitignore` and remove or comment the line `clusters/dev/lingua-greeter*.yaml` to allow these files to be checked into git.

Now commit and push the changes to trigger flux sync. Run the following command to watch the flux reconcile,

```shell
flux get kustomizations --watch
```

After few seconds you should see the reconcile logs and you should see an log like

```text
lingua-greeter  main@sha1:fec5e8ef      False   True    Applied revision: main@sha1:fec5e8ef
```

!!!note
    The `sha1:fec5e8ef` should be sha of your `$LINGUA_GREETER_APP_REPO`. You can check the sha by running the command,
    ```shell
    git rev-parse --short HEAD
    ```

Once the sync is complete you can check the `lingua-greeter` application in `demo-apps` namespace,

```shell
kubectl get pods -n demo-apps
```

```text
NAME                             READY   STATUS    RESTARTS   AGE
lingua-greeter-7f4d9d8c6-cnnkd   1/1     Running   0          57s
```

## Call Service

---8<-- "includes/call-service.md"

---8<-- "includes/service-error.md"

---8<-- "includes/abbrevations.md"

## Use Workload Identity

---8<-- "includes/configure-workload-identity.md"

### Patch Deployment and SA

Edit and update the `$DEMO_HOME/app/kustomization.yaml` as shown,

```yaml hl_lines="14 15 25 26"
---
resources:
  - https://gitlab.com/kameshsampath/lingua-greeter//config?ref=main
patches:
  # Step 1 add node selector to use workload identity enabled nodes
  - patch: |
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        name: lingua-greeter
      spec:
       template:
         spec:
            nodeSelector:
               iam.gke.io/gke-metadata-server-enabled: "true"
    target:
      kind: Deployment
      labelSelector: app=lingua-greeter
  # Step 2 - configure SA to use translator GSA
  - patch: |
      apiVersion: v1
      kind: ServiceAccount
      metadata:
        name: lingua-greeter
        annotations:
          iam.gke.io/gcp-service-account: translator@$GCP_PROJECT.iam.gserviceaccount.com
    target:
      kind: ServiceAccount
      name: lingua-greeter
      labelSelector: app=lingua-greeter
```

!!!important
    Update the `iam.gke.io/gcp-service-account` to match your settings i.e. replace the `$GCP_PROJECT` with your Google Cloud project.

Commit and push the code. The changes to `kustomization.yaml` will trigger the build of the OCI manifest image via GitLab CI/CD pipeline.

In few mins you should see flux synchronize the new image tag digest.

!!!note
    As this is a demo we used `latest` tag but for real use cases try to use `semver` ranges as described in <https://fluxcd.io/flux/cheatsheets/oci-artifacts/>

Run the following command to watch for sync,

```shell
flux get kustomizations lingua-greeter --watch
```

Wait for sync to be complete and successful, you should see logs(trimmed for brevity) like,

```shell
...
lingua-greeter  latest@sha256:d31659b9  False           True    Applied revision: latest@sha256:d31659b9
```

!!!note
    The `sha256:d31659b9`  should correspond to the image `$LINGUA_GREETER_MANIFEST_IMAGE_REPO` digest for `latest` tag.

    You can verify it by running the following command and check the `digest`

    ```shell
    gcloud artifacts docker images describe  $LINGUA_GREETER_MANIFEST_IMAGE_REPO
    ```

You can also verify the updated SA using the command,

```shell
kubectl get sa -n demo-apps lingua-greeter -ojsonpath='{.metadata.annotations}'
```

```json
{"iam.gke.io/gcp-service-account":"translator@$GCP_PROJECT.iam.gserviceaccount.com"}
```

Call service again,

---8<-- "includes/call-service.md"

---8<-- "includes/service-success.md"

[^1]: https://www.gitops.tech/
[^2]: https://opencontainers.org/

---8<-- "includes/abbrevations.md"
